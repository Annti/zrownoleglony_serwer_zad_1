# -*- coding: utf-8 -*-

import sys
import socket
import getopt
import threading


class MultithreadServer(object):
    def __init__(self, server_address, port, clients_limit):
        self.port = port
        self.clients_limit = clients_limit
        self.current_clients_lock = threading.Lock()
        self.current_clients = 0
        self.server_address = server_address
        self.server = None

    def bind(self):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind((self.server_address, self.port))
        self.server.listen(5)

        print 'Nasłuchuję na porcie: %s:%d' % (self.server_address, self.port)

    def run(self):
        while True:
            client, address = self.server.accept()
            if self.current_clients < self.clients_limit:
                self.current_clients_lock.acquire()
                self.current_clients += 1
                self.current_clients_lock.release()
                threading.Thread(
                    target=self.handle_client,
                    args=(client, address)
                ).start()
            else:
                client.close()

    def handle_client(self, client, address):
        print 'Odezwał się klient spod adresu: %s:%d' % (
            address[0], address[1]
        )
        request = client.recv(1024)

        print "Odebrano zapytanie: %s" % request

        client.send("OK")
        client.close()

        self.current_clients_lock.acquire()
        self.current_clients -= 1
        self.current_clients_lock.release()


if __name__ == '__main__':

    SERVER_ADDRESS = "localhost"
    SERVER_PORT = None
    CLIENTS_LIMIT = None

    try:
        OPTS, ARGS = getopt.getopt(sys.argv[1:], "p:c:")
    except getopt.GetoptError:
        print "Server.py -p <port> -c <clients limit>"
        sys.exit(2)

    for opt, arg in OPTS:
        if opt == '-p':
            SERVER_PORT = int(arg)
        if opt == '-c':
            CLIENTS_LIMIT = int(arg)

    SERVER = MultithreadServer(server_address=SERVER_ADDRESS,
                               port=SERVER_PORT,
                               clients_limit=CLIENTS_LIMIT)
    SERVER.bind()
    SERVER.run()
