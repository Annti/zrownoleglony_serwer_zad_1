#!/usr/bin/env python
import socket
import getopt
import sys
from threading import Thread

import time

host = 'localhost'
port = 12345
clients = 14

class ClientWorker(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        server = socket.socket()
        server.connect((host, port))
        time.sleep(2)
        server.send('GET / HTTP/1.1\r\nHost: {}\r\n\r\n'.format(host))
        print server.recv(4096)


if __name__ == '__main__':

    server_threads = [ClientWorker() for x in xrange(clients)]

    for thread in server_threads:
        thread.start()
